# EasyMedia-V2

#### 介绍
easymedia是基于javacv中封装的录制推流器，还有部分流支持不是很好，打算重新写个EasyMedia-V2基于ffmpeg，增加媒体录制保存到分布式文件系统，增加多种直播方式，flv、hls

#### 说明
此项目只是个demo，暂时不维护，目前此项目部分内容全部已经整合到EasyMedia里面了，请使用easymedia版本




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
