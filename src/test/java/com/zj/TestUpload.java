package com.zj;

import com.zj.model.AssignResult;
import com.zj.model.UploadResult;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;

public class TestUpload {

	public static void main(String[] args) {
		String string = HttpUtil.get("http://localhost:9333/dir/assign?ttl=1m");
		System.out.println(string);
		AssignResult assignResult = JSONUtil.toBean(string, AssignResult.class);
		
		HttpRequest createPost = HttpUtil.createPost("http://"+assignResult.getUrl()+"/"+assignResult.getFid());
		createPost.form("file", FileUtil.readBytes("d:/flv/out4.ts.ts"), "test.ts");
		HttpResponse execute = createPost.execute();
		System.out.println(execute.body());
		UploadResult uploadResult = JSONUtil.toBean(execute.body(), UploadResult.class);
		
	}
}
