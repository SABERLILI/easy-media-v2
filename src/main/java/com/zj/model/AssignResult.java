package com.zj.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignResult {

	private String fid;	//48,01ff9bd124
	private String url;	//192.168.2.3:8081
	private String publicUrl;	//publicUrl
	private long count;
}
