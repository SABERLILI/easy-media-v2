package com.zj.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadResult {

	private String name;
	private long size;
	private String eTag;
}
