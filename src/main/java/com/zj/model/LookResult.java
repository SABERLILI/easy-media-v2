package com.zj.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LookResult {

	private String volumeId;	//48,01ff9bd124
	private List<Localtion> locations;	//192.168.2.3:8081
	
	
	@Getter
	@Setter
	public
	class Localtion {
		private String url;
		private String publicUrl;
	}

}

