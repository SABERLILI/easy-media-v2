package com.zj.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 视频录像记录
 * </p>
 *
 * @author jzhang
 * @since 2021-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MediaBaseRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private long id;
    
    private Integer cameraId;

    private String fid;
    
    private Integer duration;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
