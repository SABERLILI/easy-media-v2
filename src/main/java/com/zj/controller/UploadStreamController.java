package com.zj.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bytedeco.javacpp.Loader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zj.service.IMediaBaseRecordService;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class UploadStreamController {

	@Autowired
	private Environment env;

	@Autowired
	private IMediaBaseRecordService mediaBaseRecordService;

	@RequestMapping("record/{cameraId}/{name}")
	public void name(HttpServletRequest request, @PathVariable("cameraId") Integer cameraId,
			@PathVariable("name") String name) throws IOException {
		System.out.println(cameraId + "--" + name);
		mediaBaseRecordService.record(cameraId, request.getInputStream());

	}

	@RequestMapping("playback")
	public void video(HttpServletResponse response, String cameraId, String beginTime, String endTime)
			throws IOException {
		if (StrUtil.isBlank(cameraId) || StrUtil.isBlank(beginTime) || StrUtil.isBlank(endTime)) {
			log.info("参数有误");
			response.setContentType("application/json");// application/x-mpegURL //video/mp2t ts;
			response.getOutputStream().write("参数有误".getBytes("utf-8"));
			response.getOutputStream().flush();
		} else {
			byte[] hls = mediaBaseRecordService.getHls(cameraId, beginTime, endTime);
			response.setContentType("application/vnd.apple.mpegurl");// application/x-mpegURL //video/mp2t ts;
			response.getOutputStream().write(hls);
			response.getOutputStream().flush();
		}

	}

	@RequestMapping("download")
	public void download(HttpServletResponse response, String cameraId, String beginTime, String endTime)
			throws IOException {

		String path = FileUtil.getTmpDirPath() + "video_"
				+ DateUtil.format(DateUtil.date(), DatePattern.PURE_DATETIME_PATTERN) + ".mp4";
		String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class);
		String url = StrUtil.format("http://localhost:{}/playback?cameraId={}&beginTime={}&endTime={}", env.getProperty("server.port"), cameraId,
				beginTime, endTime);
		String execForStr = RuntimeUtil.execForStr(ffmpeg, "-i", url, "-c", "copy", "-y", path);
		System.out.println(execForStr);

		File file = new File(path);
		byte[] readBytes = FileUtil.readBytes(file);
		if (file.exists()) {
			try {
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		response.setContentType("application/octet-stream;charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
		response.addHeader("Content-Length", "" + readBytes.length);
		response.getOutputStream().write(readBytes);
		response.getOutputStream().flush();

	}
}
