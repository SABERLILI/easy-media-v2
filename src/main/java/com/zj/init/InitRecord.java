package com.zj.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import com.zj.service.IMediaBaseRecordService;

@Service
public class InitRecord implements CommandLineRunner {

	@Autowired
	private IMediaBaseRecordService mediaBaseRecordService;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		startRecord();
	}

	public void startRecord() {
		
	}
}
