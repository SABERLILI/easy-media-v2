package com.zj.service;

import com.zj.entity.MediaBaseRecord;

import java.io.InputStream;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 视频录像记录 服务类
 * </p>
 *
 * @author jzhang
 * @since 2021-04-01
 */
public interface IMediaBaseRecordService extends IService<MediaBaseRecord> {

	public void record(Integer cameraId, InputStream in);
	
	public byte[] getHls(String cameraId, String beginTime, String endTime);
		
}
