package com.zj.mapper;

import com.zj.entity.MediaBaseRecord;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 视频录像记录 Mapper 接口
 * </p>
 *
 * @author jzhang
 * @since 2021-04-01
 */
public interface MediaBaseRecordMapper extends BaseMapper<MediaBaseRecord> {

	 List<Map<String,Object>> test();
		
}
