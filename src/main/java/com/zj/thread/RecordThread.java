package com.zj.thread;

import java.io.ByteArrayOutputStream;

import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.ffmpeg.global.avutil;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.FrameGrabber.Exception;

import com.zj.entity.Camera;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RecordThread extends Thread {
	
	/**
	 * 运行状态
	 */
	private boolean grabberStatus = false;
	private boolean recorderStatus = false;

	/**
	 * 相机
	 */
	private Camera camera;

	private String getOutput() {
		return "";
	}

	public void record() {
		FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(camera.getUrl());
		// 超时时间(15秒)
		grabber.setOption("stimoout", "15000000");
		grabber.setOption("threads", "1");
		grabber.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
		// 设置缓存大小，提高画质、减少卡顿花屏
		grabber.setOption("buffer_size", "1024000");
		// 如果为rtsp流，增加配置
		if ("rtsp".equals(camera.getUrl().substring(0, 4))) {
			// 设置打开协议tcp / udp
			grabber.setOption("rtsp_transport", "tcp");
		}

		try {
			grabber.start();
			grabberStatus = true;
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		FFmpegFrameRecorder recorder =new FFmpegFrameRecorder(getOutput(),grabber.getImageWidth(),grabber.getImageHeight(),grabber.getAudioChannels());
		
		recorder.setFormat("hls");
		
		//关于hls_wrap的说明，hls_wrap表示重复覆盖之前ts切片，这是一个过时配置，ffmpeg官方推荐使用hls_list_size 和hls_flags delete_segments代替hls_wrap
		//设置单个ts切片的时间长度（以秒为单位）。默认值为2秒
	    recorder.setOption("hls_time", "1");
	    //设置播放列表条目的最大数量。如果设置为0，则列表文件将包含所有片段，默认值为5
		recorder.setOption("hls_list_size", "1");
	    //自动删除切片，如果切片数量大于hls_list_size的数量，则会开始自动删除之前的ts切片，只保留hls_list_size个数量的切片
	    recorder.setOption("hls_flags","delete_segments");
	    recorder.setOption("hls_flags","split_by_time");
	    //ts切片自动删除阈值，默认值为1，表示早于hls_list_size+1的切片将被删除
	    recorder.setOption("hls_delete_threshold","1");
	    /*hls的切片类型：
	     * 'mpegts'：以MPEG-2传输流格式输出ts切片文件，可以与所有HLS版本兼容。
	     * 'fmp4':以Fragmented MP4(简称：fmp4)格式输出切片文件，类似于MPEG-DASH，fmp4文件可用于HLS version 7和更高版本。
	    */
	    recorder.setOption("hls_segment_type","mpegts");
	    //指定ts切片生成名称规则，按数字序号生成切片,例如'file%03d.ts'，就会生成file000.ts，file001.ts，file002.ts等切片文件
//	    recorder.setOption("hls_segment_filename", "d:/flv/eguid_blog_%03d.ts");
	    
		recorder.setFrameRate(25);//设置帧率
		recorder.setGopSize(25);//设置gop
//		recorder.setVideoQuality(1.0); //视频质量
		recorder.setVideoBitrate(1024);//码率
		recorder.setVideoOption("tune", "zerolatency");
		recorder.setVideoOption("preset", "ultrafast");
		recorder.setVideoOption("crf", "26");
//		recorder.setVideoCodecName("h264");//设置视频编码
		recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);//这种方式也可以
//		recorder.setAudioCodecName("aac");//设置音频编码，这种方式设置音频编码也可以
		recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);//设置音频编码

		try {
			recorder.start();
			grabber.flush();
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e1) {
			log.info("启动录制器失败", e1);
			e1.printStackTrace();
		} catch (Exception e1) {
			log.info("拉流器异常", e1);
			e1.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		record();
	}
}
